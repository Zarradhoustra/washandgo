package de.abelssoft.washandgo.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ListFragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import de.abelssoft.washandgo.AppData;
import de.abelssoft.washandgo.MainActivity;
import de.abelssoft.washandgo.R;
import de.abelssoft.washandgo.adapters.AppListAdapter;

import static de.abelssoft.washandgo.R.color;
import static de.abelssoft.washandgo.R.id;
import static de.abelssoft.washandgo.R.layout;

/**
 * A placeholder fragment containing a simple view.
 */
public class AppCacheFragment extends ListFragment implements OnRefreshListener, SearchView.OnQueryTextListener {
    //    private onCacheTaskFinishedListener mCallback;
    private static final String TAG = "AppListFragment";
    private AppListAdapter mAppListAdapter;
    private CacheSizeTask mCacheSizeTask;
    private TextView mTotalSizeTxtView;
    private SwipeRefreshLayout mSwipeLayout;
    private long mTotalCacheSize;


    public AppCacheFragment() {
    }

    public static AppCacheFragment newInstance() {
        AppCacheFragment fragment = new AppCacheFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        mCallback = (onCacheTaskFinishedListener) activity;
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(layout.fragment_app_cache_list, container,
                false);
        mSwipeLayout = (SwipeRefreshLayout) rootView
                .findViewById(id.swipe_container);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorScheme(color.holo_blue_bright,
                color.holo_green_light,
                color.holo_orange_light,
                color.holo_red_light);
        mSwipeLayout.setRefreshing(true);
        mAppListAdapter = new AppListAdapter(getActivity(), layout.app_cache_item);
        setListAdapter(mAppListAdapter);
        mTotalSizeTxtView = (TextView) rootView.findViewById(id.totalSize);
        Typeface lcdTypeFace = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(), "fonts/lcd.ttf");
        mTotalSizeTxtView.setTypeface(lcdTypeFace);
        Button clearBtn = (Button) rootView.findViewById(id.clear_all_btn);
        clearBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                PackageManager pm = getActivity().getApplicationContext()
                        .getPackageManager();
                // Get all methods on the PackageManager
                Method[] methods = pm.getClass().getDeclaredMethods();
                for (Method m : methods) {
                    if (m.getName().equals("freeStorageAndNotify")) {
                        try {
                            long desiredFreeStorage = Long.MAX_VALUE;
                            m.invoke(pm, desiredFreeStorage, null);
                            onRefresh();
                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                        break;
                    }
                }
            }
        });
        mCacheSizeTask = new CacheSizeTask();
        mCacheSizeTask.execute();
        return rootView;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       /* getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem > 0) {
                    Log.d(TAG, "hello");
                } else {
                    Log.d(TAG, "bye");
                    mTotalSizeTxtView.setVisibility(View.INVISIBLE);
                }
            }
        });*/

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getListView().setTextFilterEnabled(true);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setIconifiedByDefault(false);
        searchView.setSubmitButtonEnabled(false);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint(getString(R.string.query_hint));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case id.action_sort:
                mAppListAdapter.sort(new Comparator<AppData>() {
                    public int compare(AppData arg0, AppData arg1) {
                        return arg0.appName.compareTo(arg1.appName);
                    }
                });
                break;
            case id.action_sort_size:
                mAppListAdapter.sort(new Comparator<AppData>() {
                    public int compare(AppData arg0, AppData arg1) {
                        return Float.compare(arg1.appInfo, arg0.appInfo);
                    }
                });
                break;
        }
        return false;
    }

    @Override
    public void onRefresh() {
        if (!mCacheSizeTask.getStatus().equals(AsyncTask.Status.RUNNING)) {
            mCacheSizeTask = new CacheSizeTask();
            mCacheSizeTask.execute();
        }
    }

    @Override
    public void onDestroy() {
        mCacheSizeTask.cancel(true);
        Log.d(TAG, "BAM");
        super.onDestroy();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (!enter) {
            return AnimationUtils.loadAnimation(getActivity(), R.anim.scale_out);
        }

        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            getListView().clearTextFilter();
        } else {
            getListView().setFilterText(newText);
        }
        return true;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", ((AppListAdapter.ViewHolder) v.getTag()).packageName, null);
        intent.setData(uri);
        getActivity().startActivity(intent);
        super.onListItemClick(l, v, position, id);
    }

    public interface onCacheTaskFinishedListener {
        public void onCacheTaskFinished(float totalSize);
    }

    private class CacheSizeTask extends AsyncTask<Void, AppData, Void> {

        @Override
        protected void onPreExecute() {
            mAppListAdapter.clear();
            mTotalCacheSize = 0;
        }

        @Override
        protected Void doInBackground(Void... params) {
            final PackageManager packageManager = getActivity().getPackageManager();
            List<ApplicationInfo> appInfos = packageManager
                    .getInstalledApplications(PackageManager.GET_META_DATA);
            Method getPackageSizeInfo = null;
            try {
                getPackageSizeInfo = packageManager.getClass().getMethod(
                        "getPackageSizeInfo", String.class,
                        IPackageStatsObserver.class);
            } catch (NoSuchMethodException e) {
                Log.e("AsyncTask", e.toString());
            }
            final CountDownLatch doneSignal = new CountDownLatch(appInfos.size());
            for (final ApplicationInfo appInfo : appInfos) {
                try {
                    if (getPackageSizeInfo != null) {
                        getPackageSizeInfo.invoke(packageManager,
                                appInfo.packageName,
                                new IPackageStatsObserver.Stub() {
                                    @Override
                                    public void onGetStatsCompleted(
                                            PackageStats pStats, boolean succeeded) {
                                        doneSignal.countDown();
                                        long appCache = pStats.cacheSize;
                                        if (appCache > 12289) {
                                            try {
                                                String appName = appInfo.loadLabel(packageManager).toString();
                                                Drawable appIcon = packageManager.getApplicationIcon(pStats.packageName);
                                                String appPackage = pStats.packageName;
                                                mTotalCacheSize = mTotalCacheSize
                                                        + appCache;
                                                publishProgress(new AppData(appName, appCache, appIcon, appPackage));
                                            } catch (PackageManager.NameNotFoundException e) {
                                                Log.e("AsyncTask", e.toString());
                                            }
                                        }
                                    }
                                }
                        );
                    }
                } catch (Exception e) {
                    Log.e("AsyncTask", e.toString());
                }

            }
            try {
                doneSignal.await();
            } catch (InterruptedException e) {
                Log.e("AsyncTask", e.toString());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(AppData... appDatas) {
            mAppListAdapter.add(appDatas[0]);
            mTotalSizeTxtView.setText(MainActivity.humanReadableByteCount(mTotalCacheSize));
        }

        @Override
        protected void onPostExecute(Void result) {
            mSwipeLayout.setRefreshing(false);
//            mCallback.onCacheTaskFinished(mTotalCacheSize);
            Log.d(TAG, mTotalCacheSize + "");
        }

    }


}