package de.abelssoft.washandgo.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Browser;
import android.provider.CallLog;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import de.abelssoft.washandgo.R;

import static de.abelssoft.washandgo.R.color;
import static de.abelssoft.washandgo.R.id;
import static de.abelssoft.washandgo.R.layout;

public class PrivacyFragment extends Fragment implements OnRefreshListener, View.OnClickListener {
    //    private onCacheTaskFinishedListener mCallback;
    private static final String TAG = "PrivacyFragment";
    private AppSizeTask mAppSizeTask;
    private TextView mTotalSizeTxtView;
    private SwipeRefreshLayout mSwipeLayout;
    private long mTotalAppSize;
    private LinearLayout linearLayout;

    public PrivacyFragment() {
    }

    public static PrivacyFragment newInstance() {
        PrivacyFragment fragment = new PrivacyFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//      mCallback = (onCacheTaskFinishedListener) activity;
        setHasOptionsMenu(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(layout.fragment_privacy, container,
                false);
        rootView.findViewById(id.clear_all_btn).setVisibility(View.INVISIBLE);
        mSwipeLayout = (SwipeRefreshLayout) rootView
                .findViewById(id.swipe_container);
        mTotalSizeTxtView = (TextView) rootView.findViewById(id.totalSize);
        Button clearBtn = (Button) rootView.findViewById(id.clear_all_btn);
        Button uninstallBtn = (Button) rootView.findViewById(id.uninstall);
        linearLayout = (LinearLayout) rootView.findViewById(id.linearLayout);
        initSwipeLayout();

        Typeface lcdTypeFace = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(), "fonts/lcd.ttf");
        mTotalSizeTxtView.setTypeface(lcdTypeFace);

        clearBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //TODO
            }
        });
        uninstallBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO
            }
        });

        mAppSizeTask = new AppSizeTask();
        mAppSizeTask.execute();

        return rootView;

    }


    public void addView(String title, long info, Drawable drawable) {
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(layout.privacy_item, linearLayout, false);
        TextView infoView = (TextView) rowView.findViewById(R.id.cacheSize);
        TextView titleView = (TextView) rowView.findViewById(R.id.appName);
        ImageView iconView = (ImageView) rowView.findViewById(R.id.appIcon);
        CheckBox checkBox = (CheckBox) rowView.findViewById(R.id.checkBox);
        Spinner spinner = (Spinner) rowView.findViewById(id.spinner);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            spinner.setVisibility(View.GONE);
            checkBox.setVisibility(View.GONE);
        } else {
            String[] items = new String[]{"Older Then 3 months", "Older then 6 months", "All"};
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_item, items);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        }
//            checkBox.setOnCheckedChangeListener(this);
        titleView.setText(title);
        iconView.setImageDrawable(drawable);
        infoView.setText(info + "");
        rowView.setTag(title);
        rowView.setOnClickListener(this);
        linearLayout.addView(rowView);
    }


    private void initSwipeLayout() {
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorScheme(color.holo_blue_bright,
                color.holo_green_light,
                color.holo_orange_light,
                color.holo_red_light);
        mSwipeLayout.setRefreshing(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }

    @Override
    public void onRefresh() {
        if (!mAppSizeTask.getStatus().equals(AsyncTask.Status.RUNNING)) {
            mAppSizeTask = new AppSizeTask();
            mAppSizeTask.execute();
        }
    }

    @Override
    public void onDestroy() {
        mAppSizeTask.cancel(true);
        super.onDestroy();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (!enter) {
            return AnimationUtils.loadAnimation(getActivity(), R.anim.scale_out);
        }

        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public void onClick(View v) {

        switch (v.getTag().toString()) {
            case "SMS":
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.setClassName("com.android.mms", "com.android.mms.ui.ConversationList");
                startActivity(intent);
                break;
            case "Calls":
                Intent showCallLog = new Intent();
                showCallLog.setAction(Intent.ACTION_VIEW);
                showCallLog.setType(CallLog.Calls.CONTENT_TYPE);
                getActivity().startActivity(showCallLog);
                break;
//            case 2:
//                break;
            default:
                Log.e(TAG, "View unhandled");
        }

    }


    public interface onCacheTaskFinishedListener {
        public void onCacheTaskFinished(float totalSize);
    }

    private static class ItemData {
        public String name;
        public long info;
        public Drawable drawable;

        private ItemData(String name, long info, Drawable drawable) {
            this.name = name;
            this.info = info;
            this.drawable = drawable;
        }
    }

    private class AppSizeTask extends AsyncTask<Void, ItemData, Void> {


        @Override
        protected void onPreExecute() {
            linearLayout.removeAllViews();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Cursor cursor = getActivity().getContentResolver().query(Uri.parse("content://sms"), null, null, null, null);
            cursor.moveToFirst();
            int i = 0;
            do {
                i++;
//            String msgData = "";
//            for(int idx=0;idx<cursor.getColumnCount();idx++)
//            {
//                Log.d(TAG,msgData += " " + cursor.getColumnName(idx) + ":" + cursor.getString(idx));
//            }
            } while (cursor.moveToNext());
            ItemData itemData = new ItemData("SMS", i, getResources().getDrawable(android.R.drawable.ic_dialog_email));
            publishProgress(itemData);
            cursor.close();

            cursor = getActivity().getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
            cursor.moveToFirst();
            i = 0;
            do {
                i++;
            } while (cursor.moveToNext());
            cursor.close();
            itemData = new ItemData("Calls", i, getResources().getDrawable(android.R.drawable.ic_menu_call));
            publishProgress(itemData);

            cursor = Browser.getAllVisitedUrls(getActivity().getApplicationContext().getContentResolver());
            cursor.moveToFirst();
            i = 0;
            do {
                i++;
            } while (cursor.moveToNext());
            cursor.close();
            itemData = new ItemData("History", i, getResources().getDrawable(android.R.drawable.ic_menu_recent_history));
            publishProgress(itemData);

            return null;
        }

        @Override
        protected void onProgressUpdate(ItemData... itemData) {
            addView(itemData[0].name, itemData[0].info, itemData[0].drawable);
        }

        @Override
        protected void onPostExecute(Void result) {
            mSwipeLayout.setRefreshing(false);
//          mCallback.onCacheTaskFinished(mTotalAppSize);
        }
    }


}