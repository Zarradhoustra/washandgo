package de.abelssoft.washandgo.fragments;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.support.v4.app.ListFragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import de.abelssoft.washandgo.AppData;
import de.abelssoft.washandgo.MainActivity;
import de.abelssoft.washandgo.R;
import de.abelssoft.washandgo.adapters.AppListAdapter;

import static de.abelssoft.washandgo.R.color;
import static de.abelssoft.washandgo.R.id;
import static de.abelssoft.washandgo.R.layout;

public class RamManagementFragment extends ListFragment implements OnRefreshListener, SearchView.OnQueryTextListener {

    //    private onCacheTaskFinishedListener mCallback;

    private static final String TAG = "RamManagementFragment";

    private AppListAdapter mAppListAdapter;
    private ProcessListTask mProcessListTask;
    private TextView mTotalSizeTxtView;
    private SwipeRefreshLayout mSwipeLayout;
    private long mTotalAppSize;

    public RamManagementFragment() {
    }

    public static RamManagementFragment newInstance() {
        RamManagementFragment fragment = new RamManagementFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//      mCallback = (onCacheTaskFinishedListener) activity;
        setHasOptionsMenu(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(layout.fragment_ram_management, container,
                false);
        rootView.findViewById(id.clear_all_btn).setVisibility(View.INVISIBLE);
        mSwipeLayout = (SwipeRefreshLayout) rootView
                .findViewById(id.swipe_container);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorScheme(color.holo_blue_bright,
                color.holo_green_light,
                color.holo_orange_light,
                color.holo_red_light);
        mSwipeLayout.setRefreshing(true);
        mAppListAdapter = new AppListAdapter(getActivity(), layout.app_size_item);
        setListAdapter(mAppListAdapter);
        mTotalSizeTxtView = (TextView) rootView.findViewById(id.totalSize);
        Typeface lcdTypeFace = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(), "fonts/lcd.ttf");
        mTotalSizeTxtView.setTypeface(lcdTypeFace);

        final long SIZE_MB = 1048576L;

        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableRamMegs = mi.availMem / SIZE_MB;
        long totalRamMegs = mi.totalMem / SIZE_MB;
        mTotalSizeTxtView.setText(availableRamMegs + "/" + totalRamMegs + " Mb");


        Button clearBtn = (Button) rootView.findViewById(id.clear_all_btn);
        clearBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //TODO
            }
        });

        Button uninstallBtn = (Button) rootView.findViewById(id.uninstall);
        uninstallBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                long sizeFreed = 0;
                for (Integer position : mAppListAdapter.mCheckedItemsPositions) {
                    ActivityManager activityManager = (ActivityManager) getActivity().getSystemService(Service.ACTIVITY_SERVICE);
                    activityManager.killBackgroundProcesses(mAppListAdapter.getItem(position).packageName);
                    sizeFreed += mAppListAdapter.getItem(position).appInfo;
                }
                Toast.makeText(getActivity(), "Lorem " + MainActivity.humanReadableByteCount(sizeFreed) + " Ipsum", Toast.LENGTH_SHORT).show();
                if (!mProcessListTask.getStatus().equals(AsyncTask.Status.RUNNING)) {
                    mProcessListTask = new ProcessListTask();
                    mProcessListTask.execute();
                }
            }
        });
        mProcessListTask = new ProcessListTask();
        mProcessListTask.execute();
        return rootView;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
       /* getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem > 0) {
                    Log.d(TAG, "hello");
                } else {
                    Log.d(TAG, "bye");
                    mTotalSizeTxtView.setVisibility(View.INVISIBLE);
                }
            }
        });*/

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getListView().setTextFilterEnabled(true);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setIconifiedByDefault(false);
        searchView.setSubmitButtonEnabled(false);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint(getString(R.string.query_hint));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case id.action_sort:
                mAppListAdapter.sort(new Comparator<AppData>() {
                    public int compare(AppData arg0, AppData arg1) {
                        return arg0.appName.compareTo(arg1.appName);
                    }
                });
                break;
            case id.action_sort_size:
                mAppListAdapter.sort(new Comparator<AppData>() {
                    public int compare(AppData arg0, AppData arg1) {
                        return Float.compare(arg1.appInfo, arg0.appInfo);
                    }
                });
                break;
        }
        return false;
    }

    @Override
    public void onRefresh() {
        if (!mProcessListTask.getStatus().equals(AsyncTask.Status.RUNNING)) {
            mProcessListTask = new ProcessListTask();
            mProcessListTask.execute();
        }
    }

    @Override
    public void onDestroy() {
        mProcessListTask.cancel(true);
        super.onDestroy();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (!enter) {
            return AnimationUtils.loadAnimation(getActivity(), R.anim.scale_out);
        }

        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            getListView().clearTextFilter();
        } else {
            getListView().setFilterText(newText);
        }
        return true;
    }

    public interface onCacheTaskFinishedListener {
        public void onCacheTaskFinished(float totalSize);
    }


    private class ProcessListTask extends AsyncTask<Void, AppData, HashMap<String, AppData>> {


        @Override
        protected void onPreExecute() {
            mAppListAdapter.clear();
            mTotalAppSize = 0;
        }

        @Override
        protected HashMap<String, AppData> doInBackground(Void... params) {

            PackageManager packageManager = getActivity().getPackageManager();
            ActivityManager activityManager = (ActivityManager) getActivity().getSystemService(Service.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> tasks = activityManager.getRunningAppProcesses();
            HashMap<String, AppData> runningAppsMap = new HashMap<>();
            int[] processArray = getPidArray(tasks);
            Debug.MemoryInfo memoryInfos[] = activityManager.getProcessMemoryInfo(processArray);
            for (int i = 0; i < tasks.size(); i++) {
                android.app.ActivityManager.RunningAppProcessInfo runningappprocessinfo = tasks.get(i);
                if (runningappprocessinfo.pkgList.length != 0) {
                    Log.d(TAG, runningappprocessinfo.pkgList[0]);
                    try {
                        String appPackage = runningappprocessinfo.pkgList[0];
                        AppData appData = runningAppsMap.get(appPackage);
                        if (appData == null) {
                            ApplicationInfo appInfo = packageManager.getApplicationInfo(appPackage, PackageManager.GET_META_DATA);
                            if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 1) {
                                String appName = appInfo.loadLabel(packageManager).toString();
                                Drawable appIcon = packageManager.getApplicationIcon(appPackage);
                                long ramUsage = 1024 * memoryInfos[i].getTotalPss();
                                appData = new AppData(appName, ramUsage, appIcon, appPackage);
                                runningAppsMap.put(appPackage, appData);
                            }
                        } else {
                            appData.appInfo += 1024 * memoryInfos[i].getTotalPss();
                            runningAppsMap.put(appPackage, appData);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }
                }
            }

            return runningAppsMap;
        }


        private int[] getPidArray(List<ActivityManager.RunningAppProcessInfo> list) {
            int pids[] = new int[list.size()];
            for (int i = 0; i < pids.length; i++) {
                pids[i] = (list.get(i)).pid;
            }
            return pids;
        }


        @Override
        protected void onProgressUpdate(AppData... appDatas) {
            mAppListAdapter.add(appDatas[0]);
            mTotalSizeTxtView.setText(MainActivity.humanReadableByteCount(mTotalAppSize));
        }

        @Override
        protected void onPostExecute(HashMap<String, AppData> result) {
            for (AppData appData : result.values()) {
                mAppListAdapter.add(appData);
            }
            mSwipeLayout.setRefreshing(false);
//          mCallback.onCacheTaskFinished(mTotalAppSize);
        }
    }


}