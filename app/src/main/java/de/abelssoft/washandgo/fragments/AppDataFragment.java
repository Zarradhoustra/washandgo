package de.abelssoft.washandgo.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import de.abelssoft.washandgo.AppData;
import de.abelssoft.washandgo.MainActivity;
import de.abelssoft.washandgo.R;
import de.abelssoft.washandgo.adapters.AppListAdapter;

import static de.abelssoft.washandgo.R.color;
import static de.abelssoft.washandgo.R.id;
import static de.abelssoft.washandgo.R.layout;

/**
 * A placeholder fragment containing a simple view.
 */
public class AppDataFragment extends ListFragment implements OnRefreshListener, SearchView.OnQueryTextListener {
    //    private onCacheTaskFinishedListener mCallback;
    private static final String TAG = "AppListFragment";
    private AppListAdapter mAppListAdapter;
    private AppSizeTask mAppSizeTask;
    private TextView mTotalSizeTxtView;
    private SwipeRefreshLayout mSwipeLayout;
    private long mTotalAppSize;
//    private View headerView;

    public interface onCacheTaskFinishedListener {
        public void onCacheTaskFinished(float totalSize);
    }

    public AppDataFragment() {
    }

    public static AppDataFragment newInstance() {
        AppDataFragment fragment = new AppDataFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//      mCallback = (onCacheTaskFinishedListener) activity;
        setHasOptionsMenu(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(layout.fragment_app_size_list, container,
                false);
        rootView.findViewById(id.clear_all_btn).setVisibility(View.INVISIBLE);
        mSwipeLayout = (SwipeRefreshLayout) rootView
                .findViewById(id.swipe_container);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorScheme(color.holo_blue_bright,
                color.holo_green_light,
                color.holo_orange_light,
                color.holo_red_light);
        mSwipeLayout.setRefreshing(true);
        mAppListAdapter = new AppListAdapter(getActivity(), layout.app_size_item);
        setListAdapter(mAppListAdapter);
        mTotalSizeTxtView = (TextView) rootView.findViewById(id.totalSize);
        Typeface lcdTypeFace = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(), "fonts/lcd.ttf");
        mTotalSizeTxtView.setTypeface(lcdTypeFace);

        Button clearBtn = (Button) rootView.findViewById(id.clear_all_btn);
        clearBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //TODO
            }
        });

        Button uninstallBtn = (Button) rootView.findViewById(id.uninstall);
        uninstallBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SparseBooleanArray checked = getListView().getCheckedItemPositions();
                for (Integer position : mAppListAdapter.mCheckedItemsPositions) {
                    Uri packageURI = Uri.parse("package:" + mAppListAdapter.getItem(position).packageName);
                    Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
                    startActivity(uninstallIntent);
//                    Intent uninstallIntent =
//                            new Intent(Intent.ACTION_UNINSTALL_PACKAGE, Uri.parse(mAppListAdapter.getItem(position).packageName));
//                    startActivity(uninstallIntent);

                }
//                ArrayList<AppData> selectedItems = new ArrayList<AppData>();
//                for (int i = 0; i < checked.size(); i++) {
//                    // Item position in adapter
//                    int position = checked.keyAt(i);
//                    // Add sport if it is checked i.e.) == TRUE!
//                    if (checked.valueAt(i)) {
//                        Intent uninstallIntent =
//                                new Intent(Intent.ACTION_UNINSTALL_PACKAGE, Uri.parse(((AppData) getListAdapter().getItem(position)).packageName));
//                        startActivity(uninstallIntent);
//
//                        selectedItems.add((AppData) getListAdapter().getItem(position));
//                    }
//                }
            }
        });
        mAppSizeTask = new AppSizeTask();
        mAppSizeTask.execute();
        return rootView;

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
       /* getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem > 0) {
                    Log.d(TAG, "hello");
                } else {
                    Log.d(TAG, "bye");
                    mTotalSizeTxtView.setVisibility(View.INVISIBLE);
                }
            }
        });*/

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getListView().setTextFilterEnabled(true);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setIconifiedByDefault(false);
        searchView.setSubmitButtonEnabled(false);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint(getString(R.string.query_hint));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case id.action_sort:
                mAppListAdapter.sort(new Comparator<AppData>() {
                    public int compare(AppData arg0, AppData arg1) {
                        return arg0.appName.compareTo(arg1.appName);
                    }
                });
                break;
            case id.action_sort_size:
                mAppListAdapter.sort(new Comparator<AppData>() {
                    public int compare(AppData arg0, AppData arg1) {
                        return Float.compare(arg1.appInfo, arg0.appInfo);
                    }
                });
                break;
        }
        return false;
    }

    @Override
    public void onRefresh() {
        if (!mAppSizeTask.getStatus().equals(AsyncTask.Status.RUNNING)) {
            mAppSizeTask = new AppSizeTask();
            mAppSizeTask.execute();
        }
    }

    @Override
    public void onDestroy() {
        mAppSizeTask.cancel(true);
        super.onDestroy();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (!enter) {
            return AnimationUtils.loadAnimation(getActivity(), R.anim.scale_out);
        }

        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            getListView().clearTextFilter();
        } else {
            getListView().setFilterText(newText);
        }
        return true;
    }

//    @Override
//    public void onListItemClick(ListView l, View v, int position, long id) {
//
//        Intent intent = new Intent();
//        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//        Uri uri = Uri.fromParts("package", (String) v.getTag(), null);
//        intent.setData(uri);
//        getActivity().startActivity(intent);
//        super.onListItemClick(l, v, position, id);
//    }


    private class AppSizeTask extends AsyncTask<Void, AppData, Void> {

        @Override
        protected void onPreExecute() {
            mAppListAdapter.clear();
            mTotalAppSize = 0;
        }

        @Override
        protected Void doInBackground(Void... params) {
            final PackageManager packageManager = getActivity().getPackageManager();
            List<ApplicationInfo> appInfos = packageManager
                    .getInstalledApplications(PackageManager.GET_META_DATA);
            Method getPackageSizeInfo = null;
            try {
                getPackageSizeInfo = packageManager.getClass().getMethod(
                        "getPackageSizeInfo", String.class,
                        IPackageStatsObserver.class);
            } catch (NoSuchMethodException e) {
                Log.e(getClass().getSimpleName(), e.getMessage());
            }
            final CountDownLatch doneSignal = new CountDownLatch(appInfos.size());
            for (final ApplicationInfo appInfo : appInfos) {
                try {
                    if (getPackageSizeInfo != null) {
                        getPackageSizeInfo.invoke(packageManager,
                                appInfo.packageName,
                                new IPackageStatsObserver.Stub() {
                                    @Override
                                    public void onGetStatsCompleted(
                                            PackageStats pStats, boolean succeeded) {
                                        doneSignal.countDown();
                                        long appSize = pStats.dataSize + pStats.codeSize;

                                        if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 1) {
                                            try {
                                                String appName = appInfo.loadLabel(packageManager).toString();
                                                Drawable appIcon = packageManager.getApplicationIcon(pStats.packageName);
                                                String appPackage = pStats.packageName;
                                                mTotalAppSize = mTotalAppSize
                                                        + appSize;
                                                publishProgress(new AppData(appName, appSize, appIcon, appPackage));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                        );
                    }
                } catch (Exception e) {
                    Log.e("AsyncTask", e.toString());
                }

            }
            try {
                doneSignal.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(AppData... appDatas) {
            mAppListAdapter.add(appDatas[0]);
            mTotalSizeTxtView.setText(MainActivity.humanReadableByteCount(mTotalAppSize));
        }

        @Override
        protected void onPostExecute(Void result) {
            mSwipeLayout.setRefreshing(false);
//          mCallback.onCacheTaskFinished(mTotalAppSize);
            Log.d(TAG, mTotalAppSize + "");
        }
    }


}