package de.abelssoft.washandgo.fragments;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import de.abelssoft.washandgo.R;

/**
 * Created by KhaledZ on 05/05/2014.
 */
public class DashboardFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "DashBoardFragment";
    private Button wipeButton;
    private Button privacy;
    private Button ramButton;
    private Button dataButton;
    private RelativeLayout header;
    private boolean first = false;
    private TextView ramUsage;
    private TextView dataUsage;

    public DashboardFragment() {
    }


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (!enter) {
            wipeButton.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_left));
            ramButton.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_left));
            dataButton.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_right));
            privacy.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_right));
            header.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.abc_slide_out_top));
            return AnimationUtils.loadAnimation(getActivity(), R.anim.dummy);
        } else if (first) {
            wipeButton.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_left));
            ramButton.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_left));
            dataButton.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_right));
            privacy.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_right));
            header.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.abc_slide_in_top));

            return AnimationUtils.loadAnimation(getActivity(), R.anim.dummy);

        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container,
                false);
        header = (RelativeLayout) rootView.findViewById(R.id.header);
        wipeButton = (Button) rootView.findViewById(R.id.wipe_cache);
        ramButton = (Button) rootView.findViewById(R.id.ram_manager);
        privacy = (Button) rootView.findViewById(R.id.privacy_manager);
        dataButton = (Button) rootView.findViewById(R.id.app_manager);

        ramUsage = (TextView) rootView.findViewById(R.id.ramUsage);
        dataUsage = (TextView) rootView.findViewById(R.id.dataUsage);

        wipeButton.setOnClickListener(this);
        ramButton.setOnClickListener(this);
        dataButton.setOnClickListener(this);
        privacy.setOnClickListener(this);

        return rootView;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onResume() {
        super.onResume();

        final long SIZE_MB = 1048576L;

        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long availableSpace = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();
        long totaleSpace = (long) stat.getBlockCount() * (long) stat.getBlockSize();
        long availableSpaceMegs = availableSpace / SIZE_MB;
        long totalSpaceMegs = totaleSpace / SIZE_MB;
        dataUsage.setText(availableSpaceMegs + "/" + totalSpaceMegs + " Mb");

        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableRamMegs = mi.availMem / SIZE_MB;
        long totalRamMegs = mi.totalMem / SIZE_MB;
        ramUsage.setText(availableRamMegs + "/" + totalRamMegs + " Mb");


    }

    @Override
    public void onClick(View v) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, R.anim.dummy);
        first = true;
        switch (v.getId()) {
            case R.id.app_manager:
                AppDataFragment appDataFragment = new AppDataFragment();
                transaction.replace(R.id.fragment_container, appDataFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case R.id.wipe_cache:
                AppCacheFragment appCacheFragment = new AppCacheFragment();
                transaction.replace(R.id.fragment_container, appCacheFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case R.id.ram_manager:
                RamManagementFragment ramManagementFragment = new RamManagementFragment();
                transaction.replace(R.id.fragment_container, ramManagementFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case R.id.privacy_manager:
                PrivacyFragment privacyFragment = new PrivacyFragment();
                transaction.replace(R.id.fragment_container, privacyFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            default:
                Log.d(TAG, "Unhandled View");
        }
    }
}
