package de.abelssoft.washandgo.adapters;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.abelssoft.washandgo.AppData;
import de.abelssoft.washandgo.MainActivity;
import de.abelssoft.washandgo.R;

public class PrivacyAdapter extends ArrayAdapter<AppData> implements  CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "AppListAdapter";
    public ArrayList<Integer> mCheckedItemsPositions;
    private List<AppData> mOriginalValues;
    private List<String> mListItem;
    private int mLayoutResource;
    private boolean isKitKat;


    public PrivacyAdapter(Context context, int resource) {
        super(context, resource);
        isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        mLayoutResource = resource;
        mCheckedItemsPositions = new ArrayList<Integer>();
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Integer position = (Integer) buttonView.getTag();
        getItem(position).isSelectedForUninstall = isChecked;
        if (isChecked) {
            mCheckedItemsPositions.add(position);
        } else {
            mCheckedItemsPositions.remove(position);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(mLayoutResource, parent, false);

        TextView appInfo = (TextView) rowView.findViewById(R.id.cacheSize);
        TextView appName = (TextView) rowView.findViewById(R.id.appName);
        ImageView appIcon = (ImageView) rowView.findViewById(R.id.appIcon);
        CheckBox checkBox = (CheckBox) rowView.findViewById(R.id.checkBox);
        if (checkBox != null) {
            checkBox.setOnCheckedChangeListener(this);
        }
        // fill data
        appName.setText(getItem(position).appName);
        appIcon.setImageDrawable(getItem(position).appIcon);
        appInfo.setText(MainActivity.humanReadableByteCount(getItem(position).appInfo));
        if (checkBox != null) {
            checkBox.setTag(position);
            checkBox.setChecked(getItem(position).isSelectedForUninstall);
        }
        return rowView;
    }

    @Override
    public void remove(AppData object) {
        Log.d(TAG, "removed");
        super.remove(object);
    }

    @Override
    public void clear() {
        mCheckedItemsPositions = new ArrayList<Integer>();
        super.clear();
    }





}