package de.abelssoft.washandgo.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.abelssoft.washandgo.AppData;
import de.abelssoft.washandgo.MainActivity;
import de.abelssoft.washandgo.R;

public class AppListAdapter extends ArrayAdapter<AppData> implements Filterable, CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "AppListAdapter";
    public ArrayList<Integer> mCheckedItemsPositions;
    private List<AppData> mOriginalValues;
    private List<String> mListItem;
    private int mLayoutResource;

//    private CompoundButton.OnCheckedChangeListener checkListener = new CompoundButton.OnCheckedChangeListener() {
//        @Override
//        public void onCheckedChanged(CompoundButton checkboxView, boolean isChecked) {
//
//        }
//    };


    public AppListAdapter(Context context, int resource) {
        super(context, resource);
        mLayoutResource = resource;
        mCheckedItemsPositions = new ArrayList<Integer>();
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Integer position = (Integer) buttonView.getTag();
        getItem(position).isSelectedForUninstall = isChecked;
        if (isChecked) {
            mCheckedItemsPositions.add(position);
        } else {
            mCheckedItemsPositions.remove(position);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(mLayoutResource, parent, false);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.appInfo = (TextView) rowView.findViewById(R.id.cacheSize);
            viewHolder.appName = (TextView) rowView.findViewById(R.id.appName);
            viewHolder.appIcon = (ImageView) rowView.findViewById(R.id.appIcon);
            viewHolder.checkBox = (CheckBox) rowView.findViewById(R.id.checkBox);
            if (viewHolder.checkBox != null) {
                viewHolder.checkBox.setOnCheckedChangeListener(this);
            }
            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.appName.setText(getItem(position).appName);
        holder.appIcon.setImageDrawable(getItem(position).appIcon);
        holder.appInfo.setText(MainActivity.humanReadableByteCount(getItem(position).appInfo));
        holder.packageName = getItem(position).packageName;
        if (holder.checkBox != null) {
            holder.checkBox.setTag(position);
            holder.checkBox.setChecked(getItem(position).isSelectedForUninstall);
        }
        return rowView;
    }

    @Override
    public void remove(AppData object) {
        Log.d(TAG, "removed");
        super.remove(object);
    }

    @Override
    public void clear() {
        mCheckedItemsPositions = new ArrayList<Integer>();
        super.clear();
    }

    private ArrayList<AppData> getItemList() {
        ArrayList<AppData> appDatas = new ArrayList<AppData>();
        for (int i = 0; i < getCount(); i++) {
            appDatas.add(getItem(i));
        }
        return appDatas;
    }

    @Override
    public Filter getFilter() {
        /**
         * A filter object which will
         * filter message key
         * */
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                clear();
                addAll((List<AppData>) results.values); // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values. Only filtered values will be shown on the list
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation for publishing

                List<AppData> FilteredArrList = new ArrayList<AppData>();

                if (mOriginalValues == null) {
                    mOriginalValues = getItemList(); // saves the original data in mOriginalValues
                }
                setDropDownViewResource(R.id.action_bar);
                if (mListItem == null) {
                    mListItem = new ArrayList<String>();
                    for (AppData appData : mOriginalValues) {
                        mListItem.add(appData.appName);
                    }
                }
                /**
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 **/

                if (constraint == null || constraint.length() == 0) {

                    /* CONTRACT FOR IMPLEMENTING FILTER : set the Original values to result which will be returned for publishing */
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    /* Do the filtering */
                    constraint = constraint.toString().toLowerCase();

                    for (int i = 0; i < mListItem.size(); i++) {
                        String data = mListItem.get(i);
                        if (data.toLowerCase().startsWith(constraint.toString())) {
                            FilteredArrList.add(mOriginalValues.get(i));
                        }
                    }

                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }


    public static class ViewHolder {
        public String packageName;
        public TextView appInfo;
        public TextView appName;
        public ImageView appIcon;
        public CheckBox checkBox;

    }


}