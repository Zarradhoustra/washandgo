package de.abelssoft.washandgo;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class ScheduledService extends IntentService {

    private final static int NOTIFICATION_ID = 0x1;
    private long totalCacheSize;

    public ScheduledService() {
        super("ScheduledService");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(getClass().getSimpleName(), "I stopped ! cache : " + totalCacheSize);

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        totalCacheSize = 0;
        Log.d(getClass().getSimpleName(), "I ran!");
        final PackageManager packageManager = getPackageManager();
        Method getPackageSizeInfo = null;
        try {
            getPackageSizeInfo = packageManager.getClass().getMethod(
                    "getPackageSizeInfo", String.class,
                    IPackageStatsObserver.class);
        } catch (NoSuchMethodException e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
        List<ApplicationInfo> appInfos = packageManager
                .getInstalledApplications(PackageManager.GET_META_DATA);
        final CountDownLatch doneSignal = new CountDownLatch(appInfos.size());
        for (final ApplicationInfo appInfo : appInfos) {
            try {
                if (getPackageSizeInfo != null) {
                    getPackageSizeInfo.invoke(packageManager,
                            appInfo.packageName,
                            new IPackageStatsObserver.Stub() {
                                @Override
                                public void onGetStatsCompleted(
                                        PackageStats pStats, boolean succeeded) {
                                    doneSignal.countDown();
                                    long appCache = pStats.cacheSize;
                                    if (appCache > 12289) {
                                        totalCacheSize += appCache;
                                    }
                                }
                            }
                    );
                }
            } catch (Exception e) {
                Log.e("AsyncTask", e.toString());
            }
        }
        try {
            doneSignal.await();
        } catch (InterruptedException e) {
            Log.e("AsyncTask", e.toString());
        }

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.abc_ic_go)
                        .setContentTitle("My Notification Title")
                        .setContentText("Something interesting happened").setOnlyAlertOnce(true);
        Intent targetIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, targetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(NOTIFICATION_ID, builder.build());

    }
}