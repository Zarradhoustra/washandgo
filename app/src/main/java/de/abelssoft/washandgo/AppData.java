package de.abelssoft.washandgo;

import android.graphics.drawable.Drawable;

public class AppData {
    public String appName;
    public long appInfo;
    public Drawable appIcon;
    public String packageName;
    public boolean isSelectedForUninstall = false;

    /**
     * @param appName
     * @param appIcon
     * @param packageName
     */
    public AppData(String appName, Drawable appIcon, String packageName) {
        super();
        this.appName = appName;
        this.appIcon = appIcon;
        this.packageName = packageName;
    }


    /**
     * @param appName
     * @param appInfo
     * @param appIcon
     * @param packageName
     */
    public AppData(String appName, long appInfo, Drawable appIcon,
                   String packageName) {
        super();
        this.appName = appName;
        this.appInfo = appInfo;
        this.appIcon = appIcon;
        this.packageName = packageName;
    }

}
